<?php

/**
 * @param $arr
 * @return array
 * converts array of digits to number and adds an 1
 * prepended zeros get lost
 */
function addOneMakeToNumber($arr)
{
    $numberFromArray = 0;

    for ($i = 0; $i < count($arr); $i++) {
        $numberFromArray = $numberFromArray * 10;
        $numberFromArray += $arr[$i];
    }

    $numberFromArray++;

    $digitsFromNumber = array();

    while ($numberFromArray > 0) {
        $digit = $numberFromArray % 10;
        array_push($digitsFromNumber, $digit);
        $numberFromArray = intdiv($numberFromArray, 10);
    }
    return array_reverse($digitsFromNumber);
}

/**
 * @param $arr
 * @return mixed
 * adds an 1 to the last entry of an array of digits
 * if the last entry is 9 than the last entry is set to 0 and an overhead is passed to the next entry in the array
 */
function addOneArrayCheck($arr)
{
    $overhead = true;
    $i = count($arr) - 1;
    do {
        if ($overhead) {
            if ($arr[$i] == 9) {
                $arr[$i] = 0;
            } else {
                $arr[$i]++;
                return $arr;
            }
        }
        $i--;
    } while ($i >= 0);
    if ($overhead)
        array_unshift($arr, 1);
    return $arr;
}

/**
 * @param $arr
 * @return array
 * 
 */
function addOneSmallOne($arr)
{
    for ($i = count($arr) - 1; $i >= 0; $i--) {
        $arr[$i]++;
        if ($arr[$i] != 10) {
            return $arr;
        }
        $arr[$i] = 0;
    }

    if (end($arr) === 0)
        array_unshift($arr, 1);
    return $arr;
}
