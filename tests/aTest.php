<?php

require "../vendor/autoload.php";
require "../src/addOne.php";

use PHPUnit\Framework\TestCase;

class aTest extends TestCase
{
    public function testAddOne()
    {
        $testArrays = array(
            [1],
            [9],
            [1, 2, 3],
            [9, 9, 9],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 9, 2]
        );

        $checkArrays = array(
            [2],
            [1, 0],
            [1, 2, 4],
            [1, 0, 0, 0],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 2],
            [1, 9, 3]
        );

        $testLeadingZero = array(
            [0, 0, 0, 2],
            [0, 0, 0, 2, 0, 0, 0, 0],
        );

        $checkLeadingZero = array(
            [0, 0, 0, 3],
            [0, 0, 0, 2, 0, 0, 0, 1],
        );


        foreach ($testArrays as $index => $currentTest) {
            $showInputOnError = 'Input: ' . implode(',', $currentTest);
            $this->assertEquals(addOneMakeToNumber($currentTest), $checkArrays[$index], $showInputOnError);
            $this->assertEquals(addOneArrayCheck($currentTest), $checkArrays[$index], $showInputOnError);
            $this->assertEquals(addOneSmallOne($currentTest), $checkArrays[$index], $showInputOnError);
        }
        
        foreach ($testLeadingZero as $index => $currentTest) {
            $showInputOnError = 'Input: ' . implode(',', $currentTest);
            $this->assertNotEquals(addOneMakeToNumber($currentTest), $checkLeadingZero[$index], $showInputOnError); //attention
            $this->assertEquals(addOneArrayCheck($currentTest), $checkLeadingZero[$index], $showInputOnError);
            $this->assertEquals(addOneSmallOne($currentTest), $checkLeadingZero[$index], $showInputOnError);
        }
    }
}
